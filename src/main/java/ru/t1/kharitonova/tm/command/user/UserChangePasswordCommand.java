package ru.t1.kharitonova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.enumerated.Role;
import ru.t1.kharitonova.tm.exception.user.AccessDeniedException;
import ru.t1.kharitonova.tm.model.User;
import ru.t1.kharitonova.tm.util.HashUtil;
import ru.t1.kharitonova.tm.util.TerminalUtil;

public final class UserChangePasswordCommand extends AbstractUserCommand{

    @NotNull
    @Override
    public String getDescription() {
        return "Change password of current user.";
    }

    @NotNull
    @Override
    public String getName() {
        return "user-change-password";
    }

    @Override
    public void execute() {
        @NotNull final User user = serviceLocator.getAuthService().getUser();
        @Nullable final String userId = serviceLocator.getAuthService().getUserId();

        System.out.println("[USER CHANGE PASSWORD]");
        System.out.println("ENTER OLD PASSWORD:");
        @NotNull final String oldPassword = TerminalUtil.nextLine();
        @Nullable final String hash = HashUtil.salt(getPropertyService(), oldPassword);
        if (hash == null || !hash.equals(user.getPasswordHash())) throw new AccessDeniedException();

        System.out.println("ENTER NEW PASSWORD:");
        @NotNull final String newPassword = TerminalUtil.nextLine();
        serviceLocator.getUserService().setPassword(userId, newPassword);
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
