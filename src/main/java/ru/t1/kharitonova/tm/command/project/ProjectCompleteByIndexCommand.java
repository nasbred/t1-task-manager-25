package ru.t1.kharitonova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.kharitonova.tm.enumerated.Status;
import ru.t1.kharitonova.tm.util.TerminalUtil;

public final class ProjectCompleteByIndexCommand extends AbstractProjectCommand{

    @NotNull
    @Override
    public String getDescription() {
        return "Complete project by index.";
    }

    @NotNull
    @Override
    public String getName() {
        return "project-complete-by-index";
    }

    @Override
    public void execute() {
        System.out.println("[COMPLETE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber()-1;
        @NotNull final String userId = getUserId();
        getProjectService().changeProjectStatusByIndex(userId, index, Status.COMPLETED);
    }

}
