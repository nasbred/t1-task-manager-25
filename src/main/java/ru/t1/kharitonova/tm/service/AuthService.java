package ru.t1.kharitonova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.api.service.IAuthService;
import ru.t1.kharitonova.tm.api.service.IPropertyService;
import ru.t1.kharitonova.tm.api.service.IUserService;
import ru.t1.kharitonova.tm.enumerated.Role;
import ru.t1.kharitonova.tm.exception.user.*;
import ru.t1.kharitonova.tm.model.User;
import ru.t1.kharitonova.tm.util.HashUtil;

import java.util.Arrays;

public class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @Nullable
    private String userId;
    
    @NotNull
    private IPropertyService propertyService;

    public AuthService(@NotNull final  IPropertyService propertyService,
                       @NotNull final IUserService userService
    ) {
        this.propertyService = propertyService;
        this.userService = userService;
    }

    @Nullable
    @Override
    public User registry(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        return userService.create(login, password, email);
    }

    @Override
    public void login(@Nullable final String login,@Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        if (user.isLocked()) throw new AccessDeniedException();
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        if (hash == null || !hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public boolean isAuth() {
        return userId != null;
    }

    @NotNull
    @Override
    public String getUserId() {
        if (userId == null) throw new AccessDeniedException();
        return userId;
    }

    @NotNull
    @Override
    public User getUser() {
        if (!isAuth()) throw new AccessDeniedException();
        if (userId == null) throw new AccessDeniedException();
        @Nullable final User user = userService.findById(userId);
        if (user == null) throw new AccessDeniedException();
        return user;
    }

    @Override
    public void checkRoles(@Nullable final Role[] roles) {
        if(roles == null) return;
        @NotNull final User user = getUser();
        @NotNull final Role role = user.getRole();
        final boolean hasRole = Arrays.asList(roles).contains(role);
        if (!hasRole) throw new PermissionDeniedException();
    }

}
