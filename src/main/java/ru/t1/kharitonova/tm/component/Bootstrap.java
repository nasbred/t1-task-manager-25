package ru.t1.kharitonova.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.t1.kharitonova.tm.api.repository.ICommandRepository;
import ru.t1.kharitonova.tm.api.repository.IProjectRepository;
import ru.t1.kharitonova.tm.api.repository.ITaskRepository;
import ru.t1.kharitonova.tm.api.repository.IUserRepository;
import ru.t1.kharitonova.tm.api.service.*;
import ru.t1.kharitonova.tm.command.AbstractCommand;
import ru.t1.kharitonova.tm.command.project.*;
import ru.t1.kharitonova.tm.command.system.*;
import ru.t1.kharitonova.tm.command.task.*;
import ru.t1.kharitonova.tm.command.user.*;
import ru.t1.kharitonova.tm.enumerated.Role;
import ru.t1.kharitonova.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.kharitonova.tm.exception.system.CommandNotSupportedException;
import ru.t1.kharitonova.tm.model.User;
import ru.t1.kharitonova.tm.repository.CommandRepository;
import ru.t1.kharitonova.tm.repository.ProjectRepository;
import ru.t1.kharitonova.tm.repository.TaskRepository;
import ru.t1.kharitonova.tm.repository.UserRepository;
import ru.t1.kharitonova.tm.service.*;
import ru.t1.kharitonova.tm.util.TerminalUtil;

import java.lang.reflect.Modifier;
import java.util.Set;

public final class Bootstrap implements IServiceLocator{

    @NotNull
    private static final String PACKAGE_COMMANDS = "ru.t1.kharitonova.tm.command";

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final IUserService userService = new UserService(propertyService, userRepository, projectRepository, taskRepository);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    {
        @NotNull final Reflections reflections = new Reflections(PACKAGE_COMMANDS);
        @NotNull final Set<Class<? extends AbstractCommand>> classSet =
                reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classSet) {
            registry(clazz);
        }
    }

    private void initDemoData() {
        @NotNull final User userTest = userService.create("test", "test", "test@test.ru");
        @NotNull final User userUser = userService.create("user", "user", "user@user.ru");
        @NotNull final User userAdmin = userService.create("admin", "admin", Role.ADMIN);

        projectService.create(userTest.getId(), "TEST PROJECT 2", "UserTest Project 2");
        projectService.create(userTest.getId(), "TEST PROJECT 1", "UserTest Project 1");
        projectService.create(userUser.getId(), "TEST PROJECT 3", "UserUser Project 3");
        projectService.create(userUser.getId(), "TEST PROJECT 4", "UserUser Project 4");
        projectService.create(userAdmin.getId(), "TEST PROJECT 5", "UserAdmin Project 5");
        projectService.create(userAdmin.getId(), "TEST PROJECT 6", "UserAdmin Project 6");

        taskService.create(userTest.getId(), "TEST TASK 1", "DESCRIPTION 1");
        taskService.create(userTest.getId(), "TEST TASK 2", "DESCRIPTION 2");
        taskService.create(userUser.getId(), "TEST TASK 3", "DESCRIPTION 3");
        taskService.create(userUser.getId(), "TEST TASK 4", "DESCRIPTION 4");
        taskService.create(userAdmin.getId(), "TEST TASK 5", "DESCRIPTION 5");
        taskService.create(userAdmin.getId(), "TEST TASK 6", "DESCRIPTION 6");
    }

    public void run(@Nullable String[] args) {
        runArguments(args);
        initDemoData();
        initLogger();
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                @NotNull final String command = TerminalUtil.nextLine();
                runCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    @SneakyThrows
    private void registry(@NotNull final Class<? extends AbstractCommand> clazz){
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        final AbstractCommand command = clazz.newInstance();
        registry(command);
    }

    private void runArguments(@Nullable final String[] arg) {
        if (arg == null || arg.length == 0) return;
        if (arg[0] == null) return;
        runArgument(arg[0]);
        System.exit(0);
    }

    private void runCommand(@NotNull final String command) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    private void runArgument(@NotNull final String argument) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(argument);
        abstractCommand.execute();
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK MANAGER**");
        Runtime.getRuntime().addShutdownHook(new Thread(() -> loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **")));
    }

}
